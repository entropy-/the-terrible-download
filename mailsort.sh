#!/bin/bash

COUNTFILES=0
FOUNDPASS=0
PASS="Pass: porkchop"
GO=false
unset FIRST
unset SECOND
unset SWITCH
for file in $( ls -tr /home/markup/go/src/gitlab.com/localtoast/the-terrible-download/dl/new/ )
do
    file=${file%*}
    echo ${file##*/}
    COUNT=0
    filename=/home/markup/go/src/gitlab.com/localtoast/the-terrible-download/dl/new/${file##*/}
    while read -r line; do
	COUNT=$(( $COUNT + 1 ))

	if [ $GO == true ] ; then
	    echo $line >> /home/markup/go/src/gitlab.com/localtoast/the-terrible-download/downloads/01
	fi
	
	if [ $FOUNDPASS == 1 ] ; then
	   case $PASS in
	       $line)
		   echo "Password accepted"
		   rm /home/markup/go/src/gitlab.com/localtoast/the-terrible-download/downloads/01
		   GO=true
	   esac	   
	fi
	   

	case "Content-Type: text/plain; charset=utf-8" in
	    $line) 
	     echo "found trigger on line $COUNT, file $COUNTFILES"
	     FOUNDPASS=1
	     ;;
	esac
    done < "$filename"
    FOUNDPASS=0
    GO=false
    COUNTFILES=$(( $COUNTFILES + 1 ))
done
