#! /bin/bash
#this should be edited to include your bridge
#password. Then we need to add it to startup
#so it'll unlock the gnome key ring on login
#and spawn a copy of protonmail bridge

sudo python2 -c "import gnomekeyring;gnomekeyring.unlock_sync(None, 'YOUR-BRIDGE-PASSWORD');"
sudo tmux new-session -d -s mail 'protonmail-bridge --cli'

